﻿using System;
using System.Collections.Generic;

namespace AndroidTests
{
    class HiddenReference<T>
    {

        static Dictionary<int, T> table = new Dictionary<int, T>();
        static int idgen = 0;

        int id;

        public HiddenReference()
        {
            lock (table)
            {
                id = idgen++;
            }
        }

        ~HiddenReference()
        {
            lock (table)
            {
                table.Remove(id);
            }
        }

        public T Value
        {
            get { lock (table) { return table[id]; } }
            set { lock (table) { table[id] = value; } }
        }
    }}
