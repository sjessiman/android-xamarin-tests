﻿using Android.Telephony;
using System.Collections.Generic;

namespace AndroidTests
{
    public class PersonInfo
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Telephone { get; set; }
        public IEnumerable<string> Words { get; set;}

        public static string FormatPhoneNumber(string phoneNumber)
        {
            //var test = PhoneNumberUtils.FormatNumber("1234567890");//, "US");
            var num = PhoneNumberUtils.FormatNumber(phoneNumber);//, "US");
            return num;
        }
    }
}