﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Telephony;

namespace AndroidTests
{
    class SimpleRecyclerAdapter : RecyclerView.Adapter
    {
        public event EventHandler<SimpleRecyclerAdapterClickEventArgs> ItemClick;
        public event EventHandler<SimpleRecyclerAdapterClickEventArgs> ItemLongClick;
        HiddenReference<PersonInfo[]> items = new HiddenReference<PersonInfo[]>();
		public int RecordCount { get; set;  }

        Random Random { get; } = new Random();
        bool Linear { get; }

        int RandomNumber(int max) {
            return (int)Math.Floor(Random.NextDouble() * max);
        }

        public SimpleRecyclerAdapter(int numberOfEntries, bool linear)
        {
            items.Value = GenerateData(numberOfEntries);
            Linear = linear;
        }

        PersonInfo[] GenerateData(int size)
        {
            var people = new PersonInfo[size];
            for (var i = 0; i < size; i++)
            {
                var person = new PersonInfo();
                int id = i + 1;
                int addressLines = 1 + RandomNumber(3);
                person.Name = "Test name " + id;
                person.Id = id;
                person.Street = CreateStreetAddress(id, addressLines);
                person.City = "City " + id + " " + string.Join(" ", Words('c', 10));
                person.Telephone = string.Join("-", Words('n', 2));//PersonInfo.FormatPhoneNumber(id.ToString("9085500000"));
                person.Words = Words();
                people[i] = person;
            }
            return people;
        }

        IEnumerable<string> Words(char letter = 'x', int count = 20) {
            yield return Word(letter);

            while (RandomNumber(count) > 0)
                yield return Word(letter);
            
            yield break;
        }

        string Word(char letter) => new string(letter, RandomNumber(10) + 1);

        string CreateStreetAddress(int id, int lines) {
            var address = $"Street address {id} (+{lines} lines)" + string.Join(" ", Words('s', 5));
            for (var i = 1; i <= lines; i++)
                address += "\nLine " + i + " " + string.Join(" ", Words((char)i, 5));
            return address ;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            int resource = Linear ? Resource.Layout.LinearRecyclerRowView : Resource.Layout.SimpleRecyclerRowView;
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(resource, parent, false);

            var vh = new SimpleRecyclerAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            // Replace the contents of the view with that element
            var holder = viewHolder as SimpleRecyclerAdapterViewHolder;
            holder.PersonInfo = items.Value[position];

            sw.Stop();
            System.Diagnostics.Debug.WriteLine($"BindViewHolder took {sw.ElapsedMilliseconds}ms");
        }

        public override int ItemCount => items.Value.Length;

        void OnClick(SimpleRecyclerAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(SimpleRecyclerAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class SimpleRecyclerAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView Name { get; set; }
        public TextView Id { get; set; }
        public TextView Street { get; set; }
        public TextView City { get; set; }
        public TextView Telephone { get; set; }
        public TextView Words { get; set; }

        PersonInfo _person;
        public PersonInfo PersonInfo {
            get { return _person; }
            set
            {
                _person = value;
                Name.Text = value.Name;
                Id.Text = value.Id.ToString();
                Street.Text = value.Street;
                City.Text = value.City;
                Telephone.Text = value.Telephone;
                var wordList = value.Words.ToList();
                Words.Text = wordList.Count + " : " + string.Join(" ", wordList);
            }
        }

        public SimpleRecyclerAdapterViewHolder(View itemView, Action<SimpleRecyclerAdapterClickEventArgs> clickListener,
                            Action<SimpleRecyclerAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            Name = itemView.FindViewById<TextView>(Resource.Id.textViewName);
            Id = itemView.FindViewById<TextView>(Resource.Id.textViewId);
            Street = itemView.FindViewById<TextView>(Resource.Id.textViewStreet);
            City = itemView.FindViewById<TextView>(Resource.Id.textViewCity);
            Words = itemView.FindViewById<TextView>(Resource.Id.textViewWords);
            Telephone = itemView.FindViewById<TextView>(Resource.Id.textViewTelephone);

            itemView.Click += (sender, e) => clickListener(new SimpleRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new SimpleRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }

        public static string FormatPhoneNumber(string number)
        {
            var test = PhoneNumberUtils.FormatNumber("1234567890");//, "US");
            var num = PhoneNumberUtils.FormatNumber(number);//, "US");
            return num;
        }
    }

    public class SimpleRecyclerAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }

}