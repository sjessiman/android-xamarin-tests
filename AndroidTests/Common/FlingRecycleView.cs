﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using static Android.Support.V7.Widget.RecyclerView;

namespace AndroidTests
{
	public class FlingRecycleView : RecyclerView
	{
		public FlingRecycleView( Context context ) : base(context)
		{
			
		}

		public FlingRecycleView( Context context, IAttributeSet attr ) : base(context, attr)
		{
			
		}

		public FlingRecycleView(Context context, IAttributeSet attr, int def) : base(context, attr, def)
		{
			
		}

		public override bool DispatchNestedPreFling(float velocityX, float velocityY)
		{
			int direction = velocityY < 0 ? -1 : 1;

			if( !CanScrollVertically(direction) && HasNestedScrollingParent )
				return DispatchNestedFling(velocityX, velocityY, false);
			
			return base.DispatchNestedPreFling(velocityX, velocityY);
		}
	}
}