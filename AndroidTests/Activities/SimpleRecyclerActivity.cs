﻿using Android.App;
using Android.OS;
using Android.Support.V7.Widget;

namespace AndroidTests
{

	[Activity(Label = "Simple Recycler")]
	public class SimpleRecyclerActivity : Activity
	{

		RecyclerView _recyclerView;
		RecyclerView.LayoutManager _layoutManager;
		SimpleRecyclerAdapter _adapter;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here
			SetContentView(Resource.Layout.SimpleRecycler);
            bool linear = Intent.GetBooleanExtra("linear", false);
			_adapter = new SimpleRecyclerAdapter(1000, linear);

			_recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
			_recyclerView.HasFixedSize = true;
			_recyclerView.SetAdapter(_adapter);

			_layoutManager = new LinearLayoutManager(this);
			_recyclerView.SetLayoutManager(_layoutManager);
		}
	}
}
