﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;

namespace AndroidTests
{
    [Activity(Label = "Android Tests", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            Button recyclerButton = FindViewById<Button>(Resource.Id.RecyclerButton);

            recyclerButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(SimpleRecyclerActivity));
                StartActivity(intent);
            };

            Button linearRecyclerButton = FindViewById<Button>(Resource.Id.LinearRecyclerButton);

            linearRecyclerButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(SimpleRecyclerActivity));
                intent.PutExtra("linear", true);
                StartActivity(intent);
            };

			Button nested = FindViewById<Button>(Resource.Id.NestedRecyclerButton);
		
			nested.Click += (sender, e) =>
			{
				var intent = new Intent(this, typeof(NestedScrollingActivity));
				StartActivity(intent);
			};

            Button numberTestsButton = FindViewById<Button>(Resource.Id.NumberInputTestsButton);

            numberTestsButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(Activities.NumberTestsActivity));
                StartActivity(intent);
            };

            Button webViewButton = FindViewById<Button>(Resource.Id.WebViewTestsButton);

            webViewButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(Activities.WebViewActivity));
                StartActivity(intent);
            };

        }
    }
}

