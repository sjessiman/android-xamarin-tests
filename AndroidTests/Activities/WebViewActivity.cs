﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace AndroidTests.Activities
{
    //[Activity(Label = "WebViewActivity", WindowSoftInputMode = (SoftInput)((int)SoftInput.AdjustResize + (int)SoftInput.StateAlwaysHidden))]
    [Activity(Label = "WebViewActivity")]
    public class WebViewActivity : Activity
    {
        WebView _web;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //Remove title bar
            this.RequestWindowFeature(WindowFeatures.NoTitle);

            //Remove notification bar
            //this.Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

            SetContentView(Resource.Layout.WebView);

            _web = FindViewById<WebView>(Resource.Id.webView1);
            _web.Settings.JavaScriptEnabled = true;
            _web.Settings.AllowContentAccess = true;
            _web.SetWebViewClient(new WebClient());
            _web.LoadUrl("https://www.google.com");

            //var address = FindViewById<EditText>(Resource.Id.webAddress);
            //address.OnKeyDown(Keycode.Enter, OnEnter);

            TextView address = FindViewById<TextView>(Resource.Id.webAddress);
            address.Text = "https://www.google.com";
            Button go = FindViewById<Button>(Resource.Id.webGo);

            go.Click += (sender, e) =>
            {
                _web.LoadUrl(address.Text);
            };

            Button login = FindViewById<Button>(Resource.Id.login);
            login.Click += (sender, e) =>
            {
                address.Text = "http://10.118.48.155/MobileTablet_Net/login.aspx";
                _web.LoadUrl(address.Text);
                //_web.LoadUrl("http://10.118.48.155/MobileTablet_Net/login.aspx");
            };

            Button report = FindViewById<Button>(Resource.Id.customersSeen);
            report.Click += (sender, e) =>
            {
                address.Text = "http://10.118.48.155/MobileTablet_Net/reports/mobile.aspx?CDL=customerseen.cdl&Execute=1&DASHBOARD=0&REPORTID=1&HIDEFILTER=0&SHOWCHART=1&onlygrid=true&_pageid=&NotLoadReportFilter=0";
                _web.LoadUrl(address.Text);
                //_web.LoadUrl("http://10.118.48.155/MobileTablet_Net/reports/mobile.aspx?CDL=customerseen.cdl&Execute=1&DASHBOARD=0&REPORTID=1&HIDEFILTER=0&SHOWCHART=1&onlygrid=true&_pageid=&NotLoadReportFilter=0");
            };

            Button iscroll = FindViewById<Button>(Resource.Id.iScroll);
            iscroll.Click += (sender, e) =>
            {
                address.Text = "http://cubiq.org/iscroll-5";
                _web.LoadUrl(address.Text);
                //_web.LoadUrl("http://10.118.48.155/MobileTablet_Net/reports/mobile.aspx?CDL=customerseen.cdl&Execute=1&DASHBOARD=0&REPORTID=1&HIDEFILTER=0&SHOWCHART=1&onlygrid=true&_pageid=&NotLoadReportFilter=0");
            };
        }

        private bool OnEnter(Keycode keyCode, KeyEvent e)
        {
            return true;
        }
    }

    public class WebClient : WebViewClient
    {
        public override bool ShouldOverrideUrlLoading(WebView view, IWebResourceRequest request)
        {
            view.LoadUrl(request.Url.ToString());
            return false;
        }
    }
}