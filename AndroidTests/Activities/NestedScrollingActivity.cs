﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using static Android.Support.V7.Widget.RecyclerView;

namespace AndroidTests
{
	[Activity(Label = "Nested Scrolling")]
	public class NestedScrollingActivity : Activity
	{

		FlingRecycleView _recyclerView;
		LayoutManager _layoutManager;
		SimpleRecyclerAdapter _adapter;

		PersonInfo[] Names { get; set; }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here
			SetContentView(Resource.Layout.NestedScrolling);
			_adapter = new SimpleRecyclerAdapter(25, false);

			_recyclerView = FindViewById<FlingRecycleView>(Resource.Id.recyclerView);
			_recyclerView.HasFixedSize = true;
			_recyclerView.SetAdapter(_adapter);
			_recyclerView.NestedScrollingEnabled = true;

			_layoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
			_recyclerView.SetLayoutManager(_layoutManager);

			_recyclerView = FindViewById<FlingRecycleView>(Resource.Id.recyclerView2);
			_recyclerView.HasFixedSize = true;
			_recyclerView.SetAdapter(_adapter);
			_recyclerView.NestedScrollingEnabled = true;

			_layoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
			_recyclerView.SetLayoutManager(_layoutManager);
		}
	}
}